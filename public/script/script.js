import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js';
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js';

const firebaseConfig = {
  apiKey: "AIzaSyC6AtdKgNgVmc4caBvYIxiieZ4Sc9FCk7I",
  authDomain: "forotp-ba377.firebaseapp.com",
  projectId: "forotp-ba377",
  storageBucket: "forotp-ba377.appspot.com",
  messagingSenderId: "349617350215",
  appId: "1:349617350215:web:735858dda753bce4757362",
  databaseURL: "https://forotp-ba377-default-rtdb.firebaseio.com/"
};

const app = initializeApp(firebaseConfig);
const auth = getAuth();
const database = getDatabase();

var ingresoRef = document.getElementById("logInBotonId");
var registroRef = document.getElementById("botonSignInId");

ingresoRef.addEventListener("click", logInUser);
registroRef.addEventListener("click", createUser);

var correoSignIn = document.getElementById("emailId");
var ciudadRef = document.getElementById("ciudadId");
var nombreRef = document.getElementById("nombreId");
var apellidoRef = document.getElementById("apellidoId");
var contraSignIn = document.getElementById("inputContraseña");

var correoLogIn = document.getElementById("EmailLogIn");
var contraLogIn = document.getElementById("ContraseñaLogIn");


function createUser(){

  console.log("Ingreso a la función createUser");

  let correo = correoSignIn.value;
  let pass = contraSignIn.value;
      
  if((correo != '') && (pass!= '')){
    createUserWithEmailAndPassword(auth, correo, pass)
      .then((userCredential) => {                   
        const user = userCredential.user;
        const uid = user.uid; 
        console.log("Usuario: " + user + " ID:" + uid);
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
    });

    console.log("Usuario creado");

    set(ref(database, 'usuarios/' + nombreRef.value + ' ' + apellidoRef.value),{
      email: correo,
      nombre: nombreRef.value,
      apellido: apellidoRef.value,
      ciudad: ciudadRef.value
    })

  }else{
      alert("Ingresar usuario y contraseña");
  }
}

function logInUser(){

  let correo = correoLogIn.value;
  let pass = contraLogIn.value;

  signInWithEmailAndPassword(auth, correo, pass)
  .then((userCredential) => {
    const user = userCredential.user;
    window.location.href ="./edit.html";
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
    console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
  });

}


