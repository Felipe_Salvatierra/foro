import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js';
import { getAuth, onAuthStateChanged, signOut} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, onValue, push, query} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js';

const firebaseConfig = {
  apiKey: "AIzaSyC6AtdKgNgVmc4caBvYIxiieZ4Sc9FCk7I",
  authDomain: "forotp-ba377.firebaseapp.com",
  projectId: "forotp-ba377",
  storageBucket: "forotp-ba377.appspot.com",
  messagingSenderId: "349617350215",
  appId: "1:349617350215:web:735858dda753bce4757362",
  databaseURL: "https://forotp-ba377-default-rtdb.firebaseio.com/"
};


const app = initializeApp(firebaseConfig);
const auth = getAuth();

const user = auth.currentUser;

const database = getDatabase();

var nombre;


var mensajeRef = document.getElementById("IdtxtChat");
var chatRef = document.getElementById("mensajeChatId");

var enviarRef = document.getElementById("botonChatId");
enviarRef.addEventListener("click", enviarMensaje);

var logOut = document.getElementById("logOutBoton");
logOut.addEventListener("click", loggingOut);

let uid;
let correo;

onAuthStateChanged(auth, (user) => {
    if (user) {
      uid = user.uid;
      correo = user.email;
      console.log("Usuario es:" + " " + correo + "UID: " + uid);

    } else {
    }
});


function enviarMensaje(){
    let mensaje = mensajeRef.value;
    push(ref(database, "mensaje/"), {
        msg: mensaje,
        email: correo 
    })

    mensajeRef.value = "";
    console.log("Mensaje enviado. Correo: " + correo);
}

const queryMensajes = query(ref(database, 'mensaje/'));
console.log(queryMensajes);

onValue(queryMensajes, (snapshot) => {
    snapshot.forEach((childSnapshot) => {
      const childKey = childSnapshot.key;
      const childData = childSnapshot.val().msg;
      console.log(childData);
      
      chatRef.innerHTML += `
          <p>${childSnapshot.val().email}: ${childSnapshot.val().msg}</p>
      `;
    });
});

function loggingOut(){

  const auth = getAuth();
signOut(auth).then(() => {
  window.location.href ="../index.html";
}).catch((error) => {
});

}